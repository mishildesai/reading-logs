import 'package:flutter/material.dart';
import 'package:reading_logs/pages/home.dart';

void main() {
  runApp(ReadingLogs());
}

class ReadingLogs extends StatefulWidget {
  @override
  _ReadingLogsState createState() => _ReadingLogsState();
}

class _ReadingLogsState extends State<ReadingLogs> {
  @override
  Widget build(BuildContext context) {
    return HomeWidget();
  }
}
