import 'package:flutter/material.dart';
import 'package:reading_logs/model/book.dart';
import 'package:reading_logs/pages/mybooks/book_list_item.dart';

class MyBooksWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyBooksState();
}

class _MyBooksState extends State<MyBooksWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: false,
        floatingActionButton: FloatingActionButton(
          mini: false,
          clipBehavior: Clip.none,
          onPressed: () {},
          child: Icon(Icons.add_rounded),
        ),
        body: ListView.builder(
            itemCount: 25,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 50,
                child: BookListItem(Book('Book' + index.toString(), index * 10,
                    '', 'Author' + index.toString())),
              );
            },
            padding: EdgeInsets.only(
              bottom: 20,
            )));
  }
}
