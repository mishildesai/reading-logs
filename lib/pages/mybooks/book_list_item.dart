import 'package:flutter/material.dart';
import 'package:reading_logs/model/book.dart';

class BookListItem extends ListTile {
  BookListItem(Book book)
      : super(
            leading: CircleAvatar(
              foregroundImage: Image.network(
                book.getCoverURL(),
              ).image,
              onForegroundImageError: (Object object, StackTrace stackTrace) {
                return Icon(Icons.book_rounded);
              },
            ),
            title: Text(book.getTitle()),
            subtitle: Text(book.getAuthor() +
                ', ' +
                book.getPages().toString() +
                ' pages'));
}
