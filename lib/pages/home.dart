import 'package:flutter/material.dart';
import 'package:reading_logs/pages/mybooks/my_books.dart';
import 'package:reading_logs/pages/settings/settings.dart';

class HomeWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<HomeWidget> {
  List<Widget> _widgetOptions = <Widget>[
    MyBooksWidget(),
    SettingsWidget(),
  ];

  int _selectedIndex = 0;

  /// Update the selected index when it is tapped
  _onTapBottomNavigationBar(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text('Reading Logs'),
      ),
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.book_rounded), label: 'My Books'),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings_applications_rounded),
              label: 'Settings')
        ],
        onTap: _onTapBottomNavigationBar,
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blueAccent,
      ),
    ));
  }
}
