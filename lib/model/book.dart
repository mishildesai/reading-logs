class Book {
  String _id;
  String _author;
  String _coverURL;
  int _pages;
  String _title;

  /// Create a new book.
  ///
  /// - The [title] represents the title of the book. (required)
  /// - The [pages] represents the number of pages in the book. (required)
  /// - The [author] represents the author of the book. (required)
  /// - The [coverURL] represents the URL of the book cover. (required)
  Book(String id, String title, int pages, String coverURL, String author) {
    this._id = id;
    this._title = title;
    this._pages = pages;
    this._coverURL = coverURL;
    this._author = author;
  }

  /// Returns the [id] of this book. This is useful for creating unique rows in
  /// the database.
  String getId() {
    return _id;
  }

  /// Returns the [author] of this book.
  String getAuthor() {
    return _author;
  }

  /// Returns the [coverURL] of this book.
  String getCoverURL() {
    return _coverURL;
  }

  /// Returns the number of [pages] in this book.
  int getPages() {
    return _pages;
  }

  /// Returns the [title] of this book.
  String getTitle() {
    return _title;
  }
}
